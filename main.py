import requests
import json
import time

YOUTUBE_OPERATIONAL_API_INSTANCE_URL = 'https://yt.lemnoslife.com'
url = f'{YOUTUBE_OPERATIONAL_API_INSTANCE_URL}/videos'
params = {
    'part': 'qualities',
    'id': '_DbY1odyEPc',
}

numberOfSecondsToSleep = 1
while True:
    data = requests.get(url, params).json()
    print(json.dumps(data, indent = 4))
    if '1080p' in data['items'][0]['qualities']:
        break
    time.sleep(numberOfSecondsToSleep)
    numberOfSecondsToSleep *= 2

